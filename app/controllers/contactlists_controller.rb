class ContactlistsController < ApplicationController
  before_action :set_contactlist, only: %i[ show edit update destroy ]

  # GET /contactlists or /contactlists.json
  def index
    @contactlists = Contactlist.order(created_at: :desc)
  end

  # GET /contactlists/1 or /contactlists/1.json
  def show
  end

  # GET /contactlists/new
  def new
    @contactlist = Contactlist.new
  end

  # GET /contactlists/1/edit
  def edit
  end

  # POST /contactlists or /contactlists.json
  def create
    @contactlist = Contactlist.new(contactlist_params)

    respond_to do |format|
      if @contactlist.save
        format.js
        format.html { redirect_to contactlist_url(@contactlist), notice: "Contactlist was successfully created." }
        format.json { render :show, status: :created, location: @contactlist }

      else
        format.js
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @contactlist.errors, status: :unprocessable_entity }

      end
    end
  end

  # PATCH/PUT /contactlists/1 or /contactlists/1.json
  def update
    respond_to do |format|
      if @contactlist.update(contactlist_params)

        format.html { redirect_to contactlist_url(@contactlist), notice: "Contactlist was successfully updated." }
        format.json { render :show, status: :ok, location: @contactlist }
        format.js
      else
        format.js
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @contactlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contactlists/1 or /contactlists/1.json
  def destroy
    @contactlist.destroy

    respond_to do |format|
      format.html { redirect_to contactlists_url, notice: "Contactlist was successfully destroyed." }
      format.json { head :no_content }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contactlist
      @contactlist = Contactlist.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contactlist_params
      params.require(:contactlist).permit(:name, :phone, :email, :address)
    end
end
