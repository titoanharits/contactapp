json.extract! contactlist, :id, :name, :phone, :email, :address, :created_at, :updated_at
json.url contactlist_url(contactlist, format: :json)
