class Contactlist < ApplicationRecord
  validates_presence_of :name, :address
  validates_uniqueness_of :phone, :email
end
