class CreateContactlists < ActiveRecord::Migration[6.1]
  def change
    create_table :contactlists do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :address

      t.timestamps
    end
  end
end
