require "test_helper"

class ContactlistsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contactlist = contactlists(:one)
  end

  test "should get index" do
    get contactlists_url
    assert_response :success
  end

  test "should get new" do
    get new_contactlist_url
    assert_response :success
  end

  test "should create contactlist" do
    assert_difference('Contactlist.count') do
      post contactlists_url, params: { contactlist: { address: @contactlist.address, email: @contactlist.email, name: @contactlist.name, phone: @contactlist.phone } }
    end

    assert_redirected_to contactlist_url(Contactlist.last)
  end

  test "should show contactlist" do
    get contactlist_url(@contactlist)
    assert_response :success
  end

  test "should get edit" do
    get edit_contactlist_url(@contactlist)
    assert_response :success
  end

  test "should update contactlist" do
    patch contactlist_url(@contactlist), params: { contactlist: { address: @contactlist.address, email: @contactlist.email, name: @contactlist.name, phone: @contactlist.phone } }
    assert_redirected_to contactlist_url(@contactlist)
  end

  test "should destroy contactlist" do
    assert_difference('Contactlist.count', -1) do
      delete contactlist_url(@contactlist)
    end

    assert_redirected_to contactlists_url
  end
end
