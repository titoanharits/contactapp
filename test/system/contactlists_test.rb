require "application_system_test_case"

class ContactlistsTest < ApplicationSystemTestCase
  setup do
    @contactlist = contactlists(:one)
  end

  test "visiting the index" do
    visit contactlists_url
    assert_selector "h1", text: "Contactlists"
  end

  test "creating a Contactlist" do
    visit contactlists_url
    click_on "New Contactlist"

    fill_in "Address", with: @contactlist.address
    fill_in "Email", with: @contactlist.email
    fill_in "Name", with: @contactlist.name
    fill_in "Phone", with: @contactlist.phone
    click_on "Create Contactlist"

    assert_text "Contactlist was successfully created"
    click_on "Back"
  end

  test "updating a Contactlist" do
    visit contactlists_url
    click_on "Edit", match: :first

    fill_in "Address", with: @contactlist.address
    fill_in "Email", with: @contactlist.email
    fill_in "Name", with: @contactlist.name
    fill_in "Phone", with: @contactlist.phone
    click_on "Update Contactlist"

    assert_text "Contactlist was successfully updated"
    click_on "Back"
  end

  test "destroying a Contactlist" do
    visit contactlists_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Contactlist was successfully destroyed"
  end
end
